
from flask import Flask, Response, request
app = Flask(__name__)

#from celery import Celery
#app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
#app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

#celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
#celery.conf.update(app.config)

import os
#import gubbins
from datetime import datetime

@app.route("/")
def hello():
    content = get_file('index.html')
    return Response(content, mimetype="text/html")


def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))

def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        return open(src).read()
    except IOError as exc:
        return str(exc)

base_fn = 'static/saves/'

@app.route("/poll")
def polling():
    #try:
       job_id = str(request.args.get("job"))
       if job_id:## 
          with open(base_fn + job_id + '/status.json', 'r') as g: #TODO NOTE THIS IS VERY NOT SECURE - if this was opened to external would need to sanitize/change
              return Response(g.read(), mimetype="text/html")
       else:
         return Response('', mimetype="text/html") 
    
    #except:
    #   return Response('', mimetype="text/html")

#@celery.task
#def async_photo(job_id):
#    gubbins.run_photobooth(job_id)
       
import subprocess

@app.route("/takephoto")
def photo():
    job_id = datetime.now().strftime('%Y%m%d_%H%M%S')

    subprocess.Popen(["python","gubbins.py",job_id])
    #async_photo.delay(job_id)
    return Response(job_id, mimetype="text/html")
    #get starting timestamp
    #check relay is on
    #gphoto2 --capture-image-and-download -F 4 -I=2 --force-overwrite
    #check for error, if not re-do relay
    #check timestamps later than starting
    #apply filt
    #print


if __name__ == '__main__':  # pragma: no cover
    context = ('file.crt','file.key')
    app.run(host='0.0.0.0',port=8000,ssl_context=context)#, threaded=True)
