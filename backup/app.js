
var video = document.getElementById('video');
var job_id = '';


// Get access to the camera!
if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // Not adding `{ audio: true }` since we only want video now
    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
        //video.src = window.URL.createObjectURL(stream);
        video.srcObject = stream;
        video.play();
    });
}

var canvas1 = document.getElementById('canvas1');
var canvas2 = document.getElementById('canvas2');
var canvas3 = document.getElementById('canvas3');
var canvas4 = document.getElementById('canvas4');
var context1 = canvas1.getContext('2d');
var context2 = canvas2.getContext('2d');
var context3 = canvas3.getContext('2d');
var context4 = canvas4.getContext('2d');
var video = document.getElementById('video');

// Trigger photo take
document.getElementById("snap").addEventListener("click", function() {
	
});
  

var first = false;
var second = false;
var third = false;
var fourth = false;


function countdown(seconds) {
  var countdownDiv = $('.countdown');
  if(seconds == 0) {
    countdownDiv.hide();
    return;
  }

  if ((seconds > 3) && (second == false)) {
  countdownDiv.html('Get ready!');	  
  }
  else if ((seconds > 3) && (fourth == true)) {
  countdownDiv.html('Last one!');	  	  
  }
  else if (seconds > 3){
  countdownDiv.html('And again!');	  
  }
  else {
  countdownDiv.html(seconds);
  }
  seconds--;
  setTimeout(function() { countdown(seconds) }, 1000);
}


$('#snap').click( function() {
    var status;

    function poll(){
      $.get("/poll?job="+job_id, function(data){
          status = JSON.parse(data);
          //alert(status.status_code) 
	 if (status.status_code == '2')
		{ 
			//alert(status.status_code) 
			if (first == false)
			{
			countdown(7);
			$('.countdown').show();
			first = true;

				//second pic
			    setTimeout(function() {
				second = true;
				 $('.countdown').show();
			         countdown(5);
			    }, 10000);
			
			    //third pic
			    setTimeout(function() {
				 $('.countdown').show();
			         countdown(5);
				    third = true;
			
			    }, 17500);
			
			    //fourht pic
			    setTimeout(function() {
				 $('.countdown').show();
			         countdown(5);
				fourth = true;
			    }, 26300);

			     //loading - 1
			    setTimeout(function() {	
			      $("#canvas1").closest('div').addClass("polaroid")
			      $("#canvas1").closest('div').addClass("loader")
			    }, 7500);

			     //loading - 2
			    setTimeout(function() {
			      $("#canvas2").closest('div').addClass("polaroid")
			      $("#canvas2").closest('div').addClass("loader")
			    }, 15500);

			     //loading - 3
			    setTimeout(function() {
			      $("#canvas3").closest('div').addClass("polaroid")
			      $("#canvas3").closest('div').addClass("loader")
			    }, 24000);

			     //loading - 4
			    setTimeout(function() {
			      $("#canvas4").closest('div').addClass("polaroid")
			      $("#canvas4").closest('div').addClass("loader")
			    }, 32000);
			}
						
		}
          if (status.status_code == '3')
            {//location.reload();
              var img = new Image();
              img.onload = function () {
                  //context1.drawImage(img, 0, 0);
              }
              img.src = "/static/saves/"+job_id+'/0_o.jpg';
            
            
              $('#preview').show();
              context1.drawImage(img, 0, 0, 160, 120);	
              $("#canvas1").closest('div').removeClass("loader")
              $("#canvas1").closest('div').addClass("polaroid")
            }
          if (status.status_code == '4')
            {//location.reload();
              var img = new Image();
              img.onload = function () {
                  //context2.drawImage(img, 0, 0);
              }
              img.src = "/static/saves/"+job_id+'/1_o.jpg';
            
            
              $('#preview').show();
              context2.drawImage(img, 0, 0, 160, 120);
              $("#canvas2").closest('div').removeClass("loader")	      
              $("#canvas2").closest('div').addClass("polaroid")
            }
          if (status.status_code == '5')
            {//location.reload();
              var img = new Image();
              img.onload = function () {
                  //context3.drawImage(img, 0, 0);
              }
              img.src = "/static/saves/"+job_id+'/2_o.jpg';
            
            
              $('#preview').show();
              context3.drawImage(img, 0, 0, 160, 120);
              $("#canvas3").closest('div').removeClass("loader")	      
              $("#canvas3").closest('div').addClass("polaroid")
            }
          if (status.status_code == '6')
            {//location.reload();
              var img = new Image();
              img.onload = function () {
                  //context4.drawImage(img, 0, 0);
              }
              img.src = "/static/saves/"+job_id+'/3_o.jpg';
            
            
              $('#preview').show();
              context4.drawImage(img, 0, 0, 160, 120);	
              $("#canvas4").closest('div').removeClass("loader")
              $("#canvas4").closest('div').addClass("polaroid")
            }
	    
	    
		setTimeout(function() {
			location.reload();
		}, 4000);


          //do stuff  
      }); 
    }
    setInterval(function(){ poll(); }, 1000);

    $("#canvas1").closest('div').removeClass("polaroid");
    $("#canvas2").closest('div').removeClass("polaroid");
    $("#canvas3").closest('div').removeClass("polaroid");
    $("#canvas4").closest('div').removeClass("polaroid");
    context1.clearRect(0,0, 160, 120);
    context2.clearRect(0,0, 160, 120);
    context3.clearRect(0,0, 160, 120);
    context4.clearRect(0,0, 160, 120);

    $.get( "/takephoto", function( data ) {
    job_id = data;
	});
    
    //$('.countdown').show();

    //~ setTimeout(function() {
      //~ //PhotoBooth.context.drawImage(PhotoBooth.localVideo, 0, 0, 200, 150);
      //~ $('#preview').show();
	    //~ //context1.drawImage(video, 0, 0, 160, 120);	
	    //~ $("#canvas1").closest('div').addClass("polaroid")
	     //~ //$("span").parent().
    //~ }, 3000);
    
    //~ setTimeout(function() {
      //~ //PhotoBooth.context.drawImage(PhotoBooth.localVideo, 0, 0, 200, 150);
      //~ $('#preview').show();
	    //~ context2.drawImage(video, 0, 0, 160, 120);
	//~ $("#canvas2").closest('div').addClass("polaroid")	    
    //~ }, 4000);
    
    //~ setTimeout(function() {
      //~ //PhotoBooth.context.drawImage(PhotoBooth.localVideo, 0, 0, 200, 150);
      //~ $('#preview').show();
	    //~ context3.drawImage(video, 0, 0, 160, 120);	
	    //~ $("#canvas3").closest('div').addClass("polaroid")	
    //~ }, 5000);
    
    //~ setTimeout(function() {
      //~ //PhotoBooth.context.drawImage(PhotoBooth.localVideo, 0, 0, 200, 150);
      //~ $('#preview').show();
	    //~ context4.drawImage(video, 0, 0, 160, 120);	
	    //~ $("#canvas4").closest('div').addClass("polaroid")	
    //~ }, 6000);
     


});
