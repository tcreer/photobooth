import subprocess, os, sys, json
import hid
from time import sleep
from datetime import datetime
from fpdf import FPDF

class Relay(object):
    """docstring for Relay"""
    def __init__(self, idVendor=0x16c0, idProduct=0x05df):
        self.h = hid.device()
        self.h.open(idVendor, idProduct)
        self.h.set_nonblocking(1)

    def get_switch_statuses_from_report(self, report):
        switch_statuses = report[7]
        switch_statuses = [int(x) for x in list('{0:08b}'.format(switch_statuses))]
        switch_statuses.reverse()
        return switch_statuses

    def send_feature_report(self, message):
        self.h.send_feature_report(message)

    def get_feature_report(self):
        feature = 1
        length = 8
        return self.h.get_feature_report(feature, length)

    def state(self, relay, on=None):
        if on == None:
            if relay == 0:
                report = self.get_feature_report()
                switch_statuses = self.get_switch_statuses_from_report(report)
                status = []
                for s in switch_statuses:
                    status.append(bool(s))
            else:
                report = self.get_feature_report()
                switch_statuses = self.get_switch_statuses_from_report(report)
                status = bool(switch_statuses[relay-1])

            return status

        # Setter
        else:
            if relay == 0:
                if on:
                    message = [0xFE]
                else:
                    message = [0xFC]
            else:
                if on:
                    message = [0xFF, relay]
                else:
                    message = [0xFD, relay]

            self.send_feature_report(message)

# Create a relay object
relay = Relay(idVendor=0x16c0, idProduct=0x05df)
base_fn = 'static/saves/'

def press_onoff(relay):
    relay.state(0, on=True)
    sleep(0.5)
    relay.state(1, on=True)
    sleep(0.5)
    relay.state(0, on=False)
    sleep(2)

def toy_cam(fn):
    fn_out = fn[:-4] +'_o.jpg'
    cmd_2 = ["./toycamera","-i","50","-o","150","-s","100","-O","3","-I","3",fn,fn_out]
    subprocess.call(cmd_2)

def log_status(fn, data):
    with open(fn, 'w', 0) as f:
        json.dump(data, f)
        f.close()
    
def run_photobooth(job_id):
    #if datetime.strptime(job_id,'%Y%m%d_%H%M%S') < (datetime.now() - timedelta(minutes=2)):
    #    return 0    
    status = {}
    epoch_starttime = float(datetime.utcnow().strftime('%s'))
    
    os.mkdir(base_fn+job_id)
    json_fn = base_fn+job_id+'/status.json'
    
    if 1:
        status['status'] = 'started'
        status['status_code'] = '1'
        log_status(json_fn, status)
        
        cmd_1 = ["gphoto2", "--capture-image-and-download","-F","4", "-I","2", "--force-overwrite"]
    
        try:
            press_onoff(relay)
            status['status'] = 'camera on'
            status['status_code'] = '2'
            log_status(json_fn, status)
        except IOError:
            status['status'] = 'error'
            status['status_code'] = '9'
            status['user_message'] = 'One already in progress! Please wait (or admin issue)'
            log_status(json_fn, status)
            return 0
    
        p = subprocess.Popen(cmd_1,
                     stdout=subprocess.PIPE,
                     stderr=subprocess.STDOUT)
    
        errorstr1 = '*** Error: No camera found. ***'
        successstr = 'Deleting file /store_80000001/'
    
        photo_count = 0 
        while True:
          line = p.stdout.readline()
          if line != '':
            if errorstr1 in line:
               press_onoff(relay)
               p.kill()
               p = subprocess.Popen(cmd_1,
                 stdout=subprocess.PIPE,
                 stderr=subprocess.STDOUT)
            elif successstr in line:
                  fn = line[30:42] #hacky
                  print(fn)
                  print(base_fn+job_id+'/'+fn[7:])
                  os.rename(fn,base_fn+job_id+'/'+str(photo_count)+'.jpg')
                  toy_cam(base_fn+job_id+'/'+str(photo_count)+'.jpg')
                  
                  status['status'] = 'photo done'
                  status['status_code'] = str(3+ photo_count)
                  log_status(json_fn, status)

                  
                  photo_count = photo_count +1
                  
                  #DO STUFF, move image, filter it, send to webpage, etc
            
            #the real code does filtering here
            ts = float(datetime.utcnow().strftime('%s')) -  epoch_starttime
            print ">", "%.1f" % ts + line.rstrip()
            
            if photo_count == 4:
                break
            
            if float(datetime.utcnow().strftime('%s')) - epoch_starttime > 100:
                status['status'] = 'failed timeout'
                status['status_code'] = '9'            
                status['user_message'] = 'Timed out! Try again or get a supervisor.'
                log_status(json_fn, status)
                return 0
    
        images = ['0_o.jpg','1_o.jpg','2_o.jpg','3_o.jpg']
        w = 43
        h = 43
        pdf = FPDF('P','mm',[100,150])
        pdf.add_page()
        for x, image in enumerate(images):
            pdf.image(base_fn+job_id+'/'+image, x=(5+(5+w)*(x%2)), y=5+(5+h)*int(x/2), w=w, h=h)
        pdf.output(base_fn+job_id+'/'+"output.pdf", "F")
    
          #inchar = p.stdout.read(1)
          #if inchar: #neither empty string nor None
          #  print(str(inchar)) #or end=None to flush immediately
          #else:
          #  print('') #flush for implicit line-buffering
          #  break
          
      
if __name__ == '__main__':
  run_photobooth(sys.argv[1])  
  
  